<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri = "http://www.springframework.org/tags/form"
 prefix = "form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
   
      <h2>Enter Employee  Information</h2>
      <form:form method = "POST" action = "updateEmployeeList" modelAttribute="updateobj">
         <table>
            <tr>
               <td><form:label path = "empid">EmployeeID</form:label></td>
               <td><form:input path = "empid" /></td>
            </tr>
            <tr>
               <td><form:label path = "empname">EmployeeName</form:label></td>
               <td><form:input path = "empname" /></td>
            </tr>
            <tr>
               <td><form:label path = "salary">Salary</form:label></td>
               <td><form:input path = "salary" /></td>
            </tr>
            <tr>
               <td colspan = "2">
                  <input type = "submit" value = "Update"/>
               </td>
            </tr>
         </table>  
      </form:form>
  
</body>
</html>