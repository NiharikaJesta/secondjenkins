package com.example.FirstSpringBoot.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.FirstSpringBoot.model.Principle;

@Repository
public interface PrincipleRepositories extends JpaRepository<Principle,Integer>{

}
