package com.example.FirstSpringBoot.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.FirstSpringBoot.model.College;


@Repository
public interface CollegeRepositories extends JpaRepository<College,Integer>{

}
