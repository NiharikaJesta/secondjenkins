package com.example.FirstSpringBoot.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.FirstSpringBoot.model.Employee;
@Repository
public interface EmployeeRepositories extends JpaRepository<Employee,Integer>{
	
	public List<Employee> findByEmpname(String name);
	public List<Employee> findByOrderBySalaryDesc();
	public List<Employee> findByOrderBySalaryAsc();
	
	
	//public List<Employee> findBySalary();
	
	@Query("select emp from Employee emp")
	public List<Employee> getEmpList1();
	
	@Query("select emp.empname from Employee emp")
	public List<String> getEmpList(); 
	
	@Query("select emp from Employee emp where emp.salary>?1 and emp.salary<?2")
	public List<Employee> getListBySalary(double sal1,double sal2);
}
/*Repositories contains the list of all methods for database operations.
 * CrudRepository=which will just contain mbasic database operations like insert,delete
 * ,update and view
 * JpaRepository-which is sub interface of CrudRepository which will contain extra 
 * feature like pagination and sorting
 */

 