package com.example.FirstSpringBoot.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.FirstSpringBoot.model.Answer;

@Repository
public interface AnswerRepositories extends JpaRepository<Answer,Integer>{

}
