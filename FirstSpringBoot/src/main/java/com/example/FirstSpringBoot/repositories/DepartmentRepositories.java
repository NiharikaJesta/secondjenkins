package com.example.FirstSpringBoot.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.FirstSpringBoot.model.Department;
import com.example.FirstSpringBoot.model.Employee;

@Repository
public interface DepartmentRepositories extends JpaRepository<Department,Integer> {

	@Query("select d.emplist from Department d where d.deptcode=?1")
	public List<Employee> getEmpList(int deptcode);
	
	@Query("select d from Department d order by deptname Asc")   //select d from Department d order by deptname Asc
	public List<Department> getDepList();
	
	
	@Query("select d.deptcode from Department d")
	public List<Integer> getDepListForDeptId();
}
