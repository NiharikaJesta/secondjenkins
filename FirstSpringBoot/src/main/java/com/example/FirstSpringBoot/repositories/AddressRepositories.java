package com.example.FirstSpringBoot.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.FirstSpringBoot.model.Address;
import com.example.FirstSpringBoot.model.Employee;
@Repository
public interface AddressRepositories extends JpaRepository<Address,Integer>{

}
