package com.example.FirstSpringBoot.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.FirstSpringBoot.model.Question;
@Repository
public interface QuestionRepositories extends JpaRepository<Question,Integer>{

}
