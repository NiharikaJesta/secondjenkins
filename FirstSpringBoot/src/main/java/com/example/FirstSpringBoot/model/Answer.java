package com.example.FirstSpringBoot.model;

import javax.persistence.*;
import java.util.*;
@Entity
@Table(name="Answers")
public class Answer {
	public int getAnswerid() {
		return answerid;
	}
	public void setAnswerid(int answerid) {
		this.answerid = answerid;
	}
	public String getAnswertname() {
		return answertname;
	}
	public void setAnswertname(String answertname) {
		this.answertname = answertname;
	}
	public String getPostedBy() {
		return postedBy;
	}
	public void setPostedBy(String postedBy) {
		this.postedBy = postedBy;
	}
	public List<Question> getQuestionlist() {
		return questionlist;
	}
	public void setQuestionlist(List<Question> questionlist) {
		this.questionlist = questionlist;
	}
	@Id
	private int answerid;
	private String answertname;
	private String postedBy;
	@ManyToMany(cascade=CascadeType.ALL)
	private List<Question> questionlist;
	
}
