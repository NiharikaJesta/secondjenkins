package com.example.FirstSpringBoot.model;

import javax.persistence.*;
import java.util.*;
@Entity
@Table(name="Question")
public class Question {
	public int getQid() {
		return qid;
	}
	public void setQid(int qid) {
		this.qid = qid;
	}
	public String getQtitle() {
		return qtitle;
	}
	public void setQtitle(String qtitle) {
		this.qtitle = qtitle;
	}
	public List<Answer> getAnswerlist() {
		return answerlist;
	}
	public void setAnswerlist(List<Answer> answerlist) {
		this.answerlist = answerlist;
	}
	@Id
	private int qid;
	private String qtitle;
	@ManyToMany(targetEntity=Answer.class,cascade=CascadeType.ALL)
	@JoinTable(name="Question_Answer",
	joinColumns= {
			@JoinColumn(name="qid")
	},
	inverseJoinColumns= {@JoinColumn(name="answerid")})
			
	
	private List<Answer> answerlist;

}
