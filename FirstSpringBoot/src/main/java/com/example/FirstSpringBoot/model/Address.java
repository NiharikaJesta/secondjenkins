package com.example.FirstSpringBoot.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.*;
@Entity
@Table(name="AddressBoot")
public class Address {
	@Id
	private int address_id;
	
	@Column(name="City")
	private String city;
	
	@Column(name="state")
	private String state;
	
	
	
	
	public int getAddress_id() {
		return address_id;
	}
	public void setAddress_id(int address_id) {
		this.address_id = address_id;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	

}

