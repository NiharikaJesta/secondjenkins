package com.example.FirstSpringBoot.model;

import javax.persistence.*;

@Entity
@Table(name="PrincipleBoot")
public class Principle {
	@Id
	@Column(name="principle_id")
	private int principle_id;
	
	public int getPrinciple_id() {
		return principle_id;
	}

	public void setPrinciple_id(int principle_id) {
		this.principle_id = principle_id;
	}

	public String getpName() {
		return pName;
	}

	public void setpName(String pName) {
		this.pName = pName;
	}

	public String getQualification() {
		return qualification;
	}

	public void setQualification(String qualification) {
		this.qualification = qualification;
	}

	@Column(name="PName")
	private String pName;
	
	@Column(name="qualification")
	private String qualification;
	

	


}
