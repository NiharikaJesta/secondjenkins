package com.example.FirstSpringBoot.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.*;


@Table(name="Department_boot")
@Entity
public class Department {
	@Id
	private int deptcode;
	private String deptname;
	private String city;
	
	
	@OneToMany(cascade=CascadeType.ALL)
	@JoinColumn(name="dept_code")
	private List<Employee> emplist;
	
	
	
	public int getDeptcode() {
		return deptcode;
	}
	public void setDeptcode(int deptcode) {
		this.deptcode = deptcode;
	}
	public String getDeptname() {
		return deptname;
	}
	public void setDeptname(String deptname) {
		this.deptname = deptname;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public List<Employee> getEmplist() {
		return emplist;
	}
	public void setEmplist(List<Employee> emplist) {
		this.emplist = emplist;
	}
}
