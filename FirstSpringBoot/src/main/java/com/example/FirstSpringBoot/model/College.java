package com.example.FirstSpringBoot.model;

import javax.persistence.*;

@Entity
@Table(name="CollegeBoot")
public class College {
	@Id
	@Column(name="collegeId")
	private int collegeId;
	
	@Column(name="collegeName")
	private String collegeName;
	
	
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="p_id", referencedColumnName="principle_id")
	private  Principle principleObj;


	public int getCollegeId() {
		return collegeId;
	}


	public void setCollegeId(int collegeId) {
		this.collegeId = collegeId;
	}


	public String getCollegeName() {
		return collegeName;
	}


	public void setCollegeName(String collegeName) {
		this.collegeName = collegeName;
	}


	public Principle getPrincipleObj() {
		return principleObj;
	}


	public void setPrincipleObj(Principle principleObj) {
		this.principleObj = principleObj;
	}
}
