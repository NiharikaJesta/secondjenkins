package com.example.FirstSpringBoot.model;


import javax.persistence.Column;
import javax.persistence.*;
import javax.persistence.Id;

import javax.persistence.Table;

@Entity
@Table(name="employeesBoot")
public class Employee {
	@Id
	@Column(name="empid")
	private int empid;
	@Column
	private String empname;
	public Employee(int empid, String empname, double salary) {
		super();
		this.empid = empid;
		this.empname = empname;
		this.salary = salary;
	} 
	public Employee()
	{
		
	}
	private double salary;
	public int getEmpid() {
		return empid;
	}
	public void setEmpid(int empid) {
		this.empid = empid;
	}
	public String getEmpname() {
		return empname;
	}
	public void setEmpname(String empname) {
		this.empname = empname;
	}
	public double getSalary() {
		return salary;
	}
	public void setSalary(double salary) {
		this.salary = salary;
	}
//	@OneToOne(cascade=CascadeType.ALL)
//	@JoinColumn(name="add_id", referencedColumnName="address_id")
//	private Address addobj;
//	public Address getAddobj() {
//		return addobj;
//	}
//	public void setAddobj(Address addobj) {
//		this.addobj = addobj;
//	}
//	
	
	@ManyToOne
	private Department dept;
	public Department getDept() {
		return dept;
	}
	public void setDept(Department dept) {
		this.dept = dept;
	}
	
	
	
	
	@Transient
	private java.util.List<Integer> datalist;
	public java.util.List<Integer> getDatalist() {
		return datalist;
	}
	public void setDatalist(java.util.List<Integer> datalist) {
		this.datalist = datalist;
	}
}

