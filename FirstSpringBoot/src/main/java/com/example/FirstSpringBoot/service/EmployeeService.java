package com.example.FirstSpringBoot.service;

import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.FirstSpringBoot.model.Employee;
import com.example.FirstSpringBoot.repositories.EmployeeRepositories;

@Service
public class EmployeeService {

	@Autowired
	EmployeeRepositories emprep;
	public List<Employee> getData()
	{
		List<Employee> elist=emprep.findAll();
		elist.forEach(e->{
			System.out.println(e.getEmpname());
		});
		return emprep.findAll();
	}
}
