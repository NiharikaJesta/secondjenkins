package com.example.FirstSpringBoot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import java.util.*;

import com.example.FirstSpringBoot.model.Address;
import com.example.FirstSpringBoot.model.Department;
import com.example.FirstSpringBoot.model.Employee;
import com.example.FirstSpringBoot.repositories.AddressRepositories;
import com.example.FirstSpringBoot.repositories.DepartmentRepositories;
import com.example.FirstSpringBoot.repositories.EmployeeRepositories;

@Controller
public class EmployeeController {
	@Autowired
	EmployeeRepositories emprep;
	@Autowired
	AddressRepositories addrep;

	@Autowired
	DepartmentRepositories drep;

	@RequestMapping("/EmpForm")
	public ModelAndView getForm() {
		
		List<Integer> arrlist=drep.getDepListForDeptId();
		Employee emp1=new Employee();
		emp1.setDatalist(arrlist);
		
		if(arrlist!=null)
		{
			return new ModelAndView("EmployeeForm", "empFormData", emp1);
		}
		else
		{
		return new ModelAndView("EmployeeForm", "empFormData", new Employee());
		}
	}

	@RequestMapping("/addEmployee")
	public ModelAndView data(@ModelAttribute("empFormData") Employee emp) {
		Employee e = emprep.save(emp);// it will insert data in databse.
		return new ModelAndView("viewEmployee", "empFormData", e);
	}

	@RequestMapping("/RetreiveDetails")
	public ModelAndView getDetails() {
		List<Employee> emplist = emprep.findAll();
		return new ModelAndView("Details", "employeedata", emplist);
	}

	@RequestMapping("/findById")
	public ModelAndView getFormForId() {

		return new ModelAndView("EmployeeIdForm", "empiddata", new Employee());

	}

	@RequestMapping("/update")
	public ModelAndView update(@ModelAttribute("empiddata") Employee e) {

		Optional<Employee> empobj = emprep.findById(e.getEmpid());// it will insert data in databse.
		Employee efind = null;
		if (empobj.isPresent()) {
			efind = empobj.get();
		}
		return new ModelAndView("UpdateForm", "updateobj", efind);

	}

	@RequestMapping("/updateEmployeeList")
	public ModelAndView getupdatedEmployee(@ModelAttribute("updateobj") Employee updatedemp) {

		return new ModelAndView("UpdatedEmployee", "updateobj", updatedemp);

	}

	@RequestMapping("/findByIdDelete")
	public ModelAndView getFormForIdForDelete() {
		return new ModelAndView("EmployeeIdFormForDelete", "empiddataDelte", new Employee());
	}

//	@RequestMapping("/entityRelation")
//	public String saveDetails() {
//		Address add=new Address();
//		add.setAddress_id(1);
//		add.setCity("Delhi");
//		add.setState("New Delhi");
//		Employee emp=new Employee();
//		emp.setEmpid(101);
//		emp.setEmpname("Neeha");
//		emp.setSalary(80000);
//		emp.setAddobj(add);
//		emprep.save(emp);
//		return "Success";
//		
//	}

	@RequestMapping("/Dept_Emp")
	public String deptStrore() {
		Department dept = new Department();
		dept.setDeptcode(1);
		dept.setDeptname("Finanve");
		dept.setCity("Mumbai");

		Employee e = new Employee(1001, "Ram", 90000);
		Employee e1 = new Employee(1002, "Neeha", 9000000);
		Employee e2 = new Employee(1003, "Kiran", 625487);
		List<Employee> elist = new ArrayList<Employee>();
		elist.add(e);
		elist.add(e1);
		elist.add(e2);
		dept.setEmplist(elist);
		drep.save(dept);
	
		return "sucess";

	}

	@RequestMapping("/retriveEmployeeName")
	public String getData() {

		// List<Employee> elist=emprep.findByEmpname("namitha");
		// List<Employee> elist=emprep.findByOrderBySalaryDesc();
		// List<Employee> elist=emprep.findByOrderBySalaryDesc();
		// List<Employee> elist=emprep.getEmpList1();
		List<String> elist = emprep.getEmpList();
	
		System.out.println();
		if (elist != null) {
			for (String e : elist) {
				// System.out.println("Employee id is :"+e.getEmpid());
				// System.out.println("Employee name is :"+e.getEmpname());
				// System.out.println("Employee salary is:"+e.getSalary());

				System.out.println("Employee name is  :" + e);
			}
		}
		return "Success";
	}

	@RequestMapping("/GetListSalary")
	public String getListSalary() {
		List<Employee> elist = emprep.getListBySalary(1000, 100000);
		for (Employee e : elist) {
			 System.out.println("Employee id is :"+e.getEmpid());
			 System.out.println("Employee name is :"+e.getEmpname());
			 System.out.println("Employee salary is:"+e.getSalary());

		}
		return "Success";
	}

}
