package com.example.FirstSpringBoot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.FirstSpringBoot.model.College;

import com.example.FirstSpringBoot.model.Principle;
import com.example.FirstSpringBoot.repositories.CollegeRepositories;
import com.example.FirstSpringBoot.repositories.PrincipleRepositories;

@Controller
public class CollegeController {
	
	@Autowired
	CollegeRepositories crep;
	@Autowired
	PrincipleRepositories principlerep;
	@RequestMapping("/entityRelationForCollege")
	public String saveDetails() {
		Principle p=new Principle();
		p.setPrinciple_id(1);
		p.setpName("Raguvaran");
		p.setQualification("B.com");
		College clg=new College();
		clg.setCollegeId(101);
		clg.setCollegeName("Narayana College");
		clg.setPrincipleObj(p);
		
		crep.save(clg);
		return "Success!";
		
		
	}
	
}
