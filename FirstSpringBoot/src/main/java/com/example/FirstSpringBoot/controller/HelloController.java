package com.example.FirstSpringBoot.controller;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.FirstSpringBoot.model.Employee;

@Controller
public class HelloController {
	
//	@Autowired
//	 Employee empobj;
	
	Employee empobj;
	public HelloController()
	{
		empobj=new Employee();
	}
	@RequestMapping("/")
	public String welcome()
	{
		return "Hello";
	}

	@RequestMapping("/EmployeeDetails")
	public ModelAndView getEmp()
	{
		empobj.setEmpid(1);
		empobj.setEmpname("Anaita");
		empobj.setSalary(9000.0);
		
		return new ModelAndView("EmployeeDet","employeedata",empobj);
	}
	@RequestMapping("/MultiData")
	public ModelAndView getEmpDetails()
	{
		List<String> emplist=new ArrayList<String>();
		emplist.add("Java");
		emplist.add(".Net");
		emplist.add("C#");
		emplist.add("python");
		return new ModelAndView("Details","coursedata",emplist);
	}
//	@RequestMapping("/Employee")
//	public ModelAndView getForm() {
//		return new ModelAndView("EmployeeForm","empFormData",new Employee()) ;
//	}
//	@RequestMapping("/addEmployee")
//	public ModelAndView display(@ModelAttribute("empFormData") Employee emp)
//	{
//		return new ModelAndView("viewEmployee","Employee",emp);
//	}
	

}

