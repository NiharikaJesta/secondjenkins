package com.example.FirstSpringBoot.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.FirstSpringBoot.model.Department;
import com.example.FirstSpringBoot.model.Employee;
import com.example.FirstSpringBoot.repositories.DepartmentRepositories;
@Controller
public class DepartmentController {
	@Autowired
	DepartmentRepositories drep;
	
	
	@RequestMapping("/Retreive")
	public String empList() {
		List<Employee> eList=drep.getEmpList(1);
		for (Employee e : eList) {
			 System.out.println("Employee id is :"+e.getEmpid());
			 System.out.println("Employee name is :"+e.getEmpname());
			 System.out.println("Employee salary is:"+e.getSalary());

		}
		return "Success";
	}

	@RequestMapping("/RetreiveByOrderByName")
	public String deptList() {
		List<Department> dlist=drep.getDepList();
		for(Department d:dlist)
		{
			System.out.println("Depcode :"+d.getDeptcode());
			System.out.println("City    :"+d.getCity());
			System.out.println("DeptName:"+d.getDeptname());
		}
		return "Success";
	}
}
