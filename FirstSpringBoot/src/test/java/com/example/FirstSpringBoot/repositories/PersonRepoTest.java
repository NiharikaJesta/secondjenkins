package com.example.FirstSpringBoot.repositories;

import static org.junit.Assert.assertNotNull;
import static org.assertj.core.api.Assertions.assertThat;


import java.util.List;


import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.FirstSpringBoot.model.Employee;
import com.example.FirstSpringBoot.service.EmployeeService;

@RunWith(SpringRunner.class)
@SpringBootTest
@ComponentScan(basePackages = {"com.example.FirstSpringBoot.repositories"})
//This package is specifiying the code related to repositories

class PersonRepoTest {
 
	@Autowired
	ApplicationContext context;
	//This is initilizing application context so that u can access any object.
	@Autowired
	EmployeeService empservice;
    @MockBean
    private EmployeeRepositories personRepo;
    @Test
    void contextLoads(ApplicationContext context) {
      assertThat(context).isNotNull();
    }
 
    @Test
    void isPersonExitsById() {
    	List<Employee> emplist=empservice.getData();
    	System.out.println("Size of list is "+emplist.size());
    	
    	assertNotNull(emplist);
   }

}
